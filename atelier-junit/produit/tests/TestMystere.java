import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Before;
import org.junit.Test;

public class TestMystere {
    private Chose[] terminaux;
    private Chose petit, moyen, grand;
    
    @Before
    public void genererExemples () {
        genererTerminaux();
        
        // (1+2)
        petit = new Chose('+', terminaux[1], terminaux[2]);
        
        // ((1+2)*3)
        moyen = new Chose('*', petit, terminaux[3]);
        
        // ((4-5)*((1+2)*3))
        grand = new Chose('*', new Chose('-', terminaux[4], terminaux[5]), moyen);
    }
    
    private void genererTerminaux () {
        terminaux = new Chose[6];
        terminaux[0] = new Chose('+'); // Chose invalide
        for (byte i = 1; i <= 5; i++)
            terminaux[i] = new Chose(Character.forDigit(i, 10));
    }
    
    @Test
    public void miniMystere () {
        Mystere m = new Mystere(terminaux[1]);
        assertEquals("1", m.magie());
    }
    
    @Test (expected = RuntimeException.class)
    public void miniMystereIncomplet () {
        Mystere m = new Mystere(null);
        m.magie();
    }    

    @Test (expected = RuntimeException.class)
    public void miniMystereInvalide () {
        Mystere m = new Mystere(terminaux[0]);
        m.magie();
    }
    
    @Test
    public void petitMystere () {
        Mystere m = new Mystere(petit);
        assertEquals("(1+2)", m.magie());
    }
    
    @Test (expected = RuntimeException.class)
    public void petitMystereIncomplet () {
        petit.beta = null; // (1+null)
        Mystere m = new Mystere(petit);
        m.magie();
    }

    @Test (expected = RuntimeException.class)
    public void petitMystereInvalide1 () {
        terminaux[2].gamma = '*'; // (1+*)
        Mystere m = new Mystere(petit);
        m.magie();
    }
    
    @Test (expected = RuntimeException.class)
    public void petitMystereInvalide2 () {
        petit.gamma = '9'; // (192)
        Mystere m = new Mystere(petit);
        m.magie();
    }
    
    @Test
    public void moyenMystere () {
        Mystere m = new Mystere(moyen);
        assertEquals("((1+2)*3)", m.magie());
    }
    
    @Test (expected = RuntimeException.class)
    public void moyenMystereIncomplet () {
        petit.beta = null; // ((1+null)*3)
        Mystere m = new Mystere(moyen);
        m.magie();
    }
    
    @Test (expected = RuntimeException.class)
    public void moyenMystereInvalide1 () {
        terminaux[2].gamma = '*'; // ((1+*)*3)
        Mystere m = new Mystere(moyen);
        m.magie();
    }
    
    @Test (expected = RuntimeException.class)
    public void moyenMystereInvalide2 () {
        petit.gamma = '9'; // ((192)*3)
        Mystere m = new Mystere(moyen);
        m.magie();
    }
    
    @Test (timeout = 5)
    public void grandMystere () {
        Mystere m = new Mystere(grand);
        assertEquals("((4-5)*((1+2)*3))", m.magie());
    }
    
    @Test (timeout = 5)
    public void megaMystere () {
        petit.beta = new Chose('*', new Chose('6'), new Chose('7'));
        moyen.beta = new Chose('-', new Chose('8'), new Chose('9'));
        grand.alpha.alpha = new Chose('*', new Chose('2'), new Chose('3'));
        Mystere m = new Mystere(grand);
        assertEquals("(((2*3)-5)*((1+(6*7))*(8-9)))", m.magie());
    }
}
