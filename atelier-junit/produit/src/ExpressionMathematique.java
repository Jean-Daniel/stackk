import java.util.Stack;

public class Mystere {
    private Chose laChose;
    private StringBuilder incantation;
    
    public Mystere (Chose uneChose) {
        this.laChose = uneChose;
    }
    
    public String magie () {
        incantation = new StringBuilder();
        final Chose marqueurMachin = new Chose(')');
        Stack<Chose> chosesIncompletes = new Stack<>();
        
        Chose choseCourante = laChose;
        do {
            if (estBizarre(choseCourante)) {
                augmenterMagie(choseCourante.gamma);
                
                do {
                    if (chosesIncompletes.isEmpty())
                        return incantation.toString();
                    choseCourante = chosesIncompletes.pop();
                    diminuerMagie(choseCourante.gamma);
                } while (choseCourante == marqueurMachin);
                
                choseCourante = choseCourante.beta;
            }
            else {
                incantation.append('(');
                chosesIncompletes.push(marqueurMachin);
                chosesIncompletes.push(choseCourante);
                choseCourante = choseCourante.alpha;
            }
        } while (choseCourante != null);
        
        throw new RuntimeException("Le truc a imprimer est incomplet.");
    }
    
    private boolean estBizarre (Chose c) {
        return c.alpha == null && c.beta == null;
    }
    
    private void augmenterMagie (char c) {
        if (Character.isDigit(c))
            incantation.append(c);
        else
            throw new RuntimeException("La chose " + c + " n'est pas un chiffre.");
    }
    
    private void diminuerMagie (char c) {
        if (!Character.isDigit(c))
            incantation.append(c);
        else
            throw new RuntimeException("La chose " + c + " est un chiffre.");
    }
}
