public class Chose {
    public Chose alpha, beta;
    public char gamma;
    
    public Chose (char gamma, Chose alpha, Chose beta) {
        this.alpha = alpha;
        this.beta = beta;
        this.gamma = gamma;
    }
    
    public Chose (char gamma) {
        this.alpha = null;
        this.beta = null;
        this.gamma = gamma;
    }
}
